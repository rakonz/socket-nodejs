"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.postMensajeSpecific = exports.postMensaje = exports.getMensaje = void 0;
const getMensaje = (req, res) => {
    res.json({
        ok: true,
        msg: "Everything is alright",
    });
};
exports.getMensaje = getMensaje;
const postMensaje = (req, res) => {
    const cuerpo = req.body.cuerpo;
    const de = req.body.de;
    res.json({
        ok: true,
        msg: "Post ok",
        cuerpo,
        de,
    });
};
exports.postMensaje = postMensaje;
const postMensajeSpecific = (req, res) => {
    const { id } = req.params;
    const cuerpo = req.body.cuerpo;
    const de = req.body.de;
    res.json({
        ok: true,
        msg: "Post ok",
        cuerpo,
        de,
        id,
    });
};
exports.postMensajeSpecific = postMensajeSpecific;
