"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const mensaje_1 = __importDefault(require("../routes/mensaje"));
class Server {
    constructor() {
        this.apiPaths = {
            mensajes: "/api/mensajes",
        };
        this.app = (0, express_1.default)();
        this.port = process.env.PORT || "3000";
        this.middlewares();
        this.routes();
    }
    middlewares() {
        this.app.use((0, cors_1.default)({ origin: true, credentials: true }));
        this.app.use(express_1.default.json());
        this.app.use(express_1.default.urlencoded({ extended: true }));
        this.app.use(express_1.default.static("src/public"));
    }
    routes() {
        this.app.use(this.apiPaths.mensajes, mensaje_1.default);
    }
    listen() {
        this.app.listen(this.port, () => {
            console.log("Servidor corriendo en puerto " + this.port);
        });
    }
}
exports.default = Server;
