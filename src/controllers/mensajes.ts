import { Request, Response } from "express";

export const getMensaje = (req: Request, res: Response) => {
  res.json({
    ok: true,
    msg: "Everything is alright",
  });
};
export const postMensaje = (req: Request, res: Response) => {
  const cuerpo = req.body.cuerpo;
  const de = req.body.de;

  res.json({
    ok: true,
    msg: "Post ok",
    cuerpo,
    de,
  });
};
export const postMensajeSpecific = (req: Request, res: Response) => {
  const { id } = req.params;
  const cuerpo = req.body.cuerpo;
  const de = req.body.de;

  res.json({
    ok: true,
    msg: "Post ok",
    cuerpo,
    de,
    id,
  });
};
