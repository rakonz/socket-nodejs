import { Router } from "express";
import { getMensaje, postMensaje, postMensajeSpecific } from "../controllers/mensajes";

const router = Router();

router.get("/", getMensaje);
router.post("/", postMensaje); 
router.post("/:id", postMensajeSpecific); 


export default router;
