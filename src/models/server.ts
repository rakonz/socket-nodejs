import express from "express";
import cors from "cors";
import mensajeRoutes from "../routes/mensaje";

export default class Server {
  private app: express.Application;
  private port: string;
  private apiPaths = {
    mensajes: "/api/mensajes",
  };

  constructor() {
    this.app = express();
    this.port = process.env.PORT || "3000";

    this.middlewares();
    this.routes();
  }

  middlewares() {
    this.app.use(cors({ origin: true, credentials: true }));
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: true }));
    this.app.use(express.static("src/public"));
  }

  routes() {
    this.app.use(this.apiPaths.mensajes, mensajeRoutes);
  }

  listen() {
    this.app.listen(this.port, () => {
      console.log("Servidor corriendo en puerto " + this.port);
    });
  }
}
